provider "aws" {
  profile = "terraform"
  region  = "ap-south-1"
}

resource "aws_ebs_volume" "example_volume" {
  availability_zone = "ap-south-1a"  # Replace with your desired AZ
  size              = 10  # Size of the volume in GB
  type              = "gp2"  # Volume type (gp2, io1, st1, sc1, standard)
  tags = {
    Name = "example-volume"
  }
}

output "volume_id" {
  value = aws_ebs_volume.example_volume.id
}
