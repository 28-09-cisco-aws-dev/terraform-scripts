provider "aws" {
  profile = "terraform"
  region  = "ap-south-1"  # Replace with your desired region
}

resource "aws_launch_configuration" "example_lc" {
  name_prefix = "example-lc-"
  image_id    = "ami-06f621d90fa29f6d0"  # Replace with your desired AMI ID
  instance_type = "t2.micro"

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello from User Data!"
              EOF
}

resource "aws_autoscaling_group" "example_asg" {
  name                 = "example-asg"
  min_size             = 2
  max_size             = 4
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.example_lc.name
  availability_zones  = ["ap-south-1a", "ap-south-1b"]  # Replace with desired AZs

  tag {
    key                 = "Name"
    value               = "example-instance"
    propagate_at_launch = true
  }
}


output "instance_ips" {
  value = aws_instance.example_instances[*].public_ip
}

output "instance_azs" {
  value = aws_instance.example_instances[*].availability_zone
}

resource "aws_instance" "example_instances" {
  count = aws_autoscaling_group.example_asg.desired_capacity
  ami           = "ami-06f621d90fa29f6d0"  # Replace with your desired AMI ID
  instance_type = "t2.micro"
}