provider "aws" {
  profile = "terraform"
  region = "ap-south-1" # Change this to your desired region
}

resource "aws_security_group" "instance_sg" {
  name        = "InstanceSecurityGroup"
  description = "Security group for EC2 instance"
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Be cautious with this setting; restrict to trusted IPs in production
  }
}

data "aws_key_pair" "existing_keypair" {
  key_name = "pradeep-ec2-keypair"
}

resource "aws_instance" "example_instance" {
  ami           = "ami-06f621d90fa29f6d0"  # Change this to the desired AMI ID
  instance_type = "t2.micro"              # Change this to the desired instance type

  vpc_security_group_ids = [aws_security_group.instance_sg.id]
  key_name               = data.aws_key_pair.existing_keypair.key_name

  tags = {
    Name = "Pradeep-EC2-instance"
  }
}
