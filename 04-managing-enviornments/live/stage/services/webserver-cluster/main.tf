terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "ap-south-1"

  # Allow any 2.x version of the AWS provider
  version = "~> 2.0"
}

module "webserver_cluster" {
  source = "git@gitlab.com:classpathio-terraform/webserver-shared-modules//services/webserver-cluster?ref=v0.0.3"

  cluster_name = "webserver-cluster-stage"
  remote_state_3_bucket = "classpath-application-state"
  db_remote_state_key = "stage/data-storage/mysql/terraform.tfstate"
}

