output "db_host" {
  value = aws_db_instance.stage_db.address
  description = "The DB address"
}
output "db_port" {
  value = aws_db_instance.stage_db.port
  description = "The DB port"
}

