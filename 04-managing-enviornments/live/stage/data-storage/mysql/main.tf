terraform {
    backend "s3" {
        bucket = "classpath-application-state"
        key = "stage/data-storage/mysql/terraform.tfstate"
        region = "ap-south-1"
        dynamodb_table = "terraform-managed-lock-table"
        encrypt = true
    }
}
provider "aws" {
  region = "ap-south-1"
}

resource "aws_db_instance" "stage_db" {
    identifier_prefix = "terraform-managed-db"
    engine = "mysql"
    allocated_storage = 10
    instance_class = "db.t2.micro"
    name = "emp_db"
    username = "admin"
    password = var.db_password
}

