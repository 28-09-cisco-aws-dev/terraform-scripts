
output "s3_bucket_arn" {
  value = aws_s3_bucket.application_state.arn
  description = "ARN of the S3 bucket"
}

output "dynamodb_table_name" {
  value = aws_dynamodb_table.terraform_lock_table.name
  description = "Database table name"
}