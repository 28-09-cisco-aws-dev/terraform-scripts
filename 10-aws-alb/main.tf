provider "aws" {
  profile = "terraform"
  region  = "ap-south-1"  # Replace with your desired region
}

resource "aws_launch_configuration" "example_lc" {
  name_prefix = "example-lc-"
  image_id    = "ami-06f621d90fa29f6d0"  # Replace with your desired AMI ID
  instance_type = "t2.micro"

  user_data = <<-EOF
              #!/bin/bash
              echo "Hello from User Data!"
              EOF
}

resource "aws_autoscaling_group" "example_asg" {
  name                 = "example-asg"
  min_size             = 2
  max_size             = 4
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.example_lc.name
  availability_zones   = ["ap-south-1a", "ap-south-1b"]  # Replace with desired AZs

  tag {
    key                 = "Name"
    value               = "example-instance"
    propagate_at_launch = true
  }
}

resource "aws_instance" "example_instances" {
  count = aws_autoscaling_group.example_asg.desired_capacity
  ami           = "ami-06f621d90fa29f6d0"  # Replace with your desired AMI ID
  instance_type = "t2.micro"
}

resource "aws_security_group" "example_lb_security_group" {
  name        = "example-lb-security-group"
  description = "Security group for Load Balancer"
  
  # Allow incoming HTTP traffic
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "example_lb" {
  name               = "example-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups   = [aws_security_group.example_lb_security_group.id]
  subnets            = ["subnet-0d13d4c9b857fe3fa", "subnet-0afc1b21c98f5babe"]  # Replace with your subnet IDs
}

resource "aws_lb_target_group" "example_target_group" {
  name     = "example-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "vpc-048bb062b7add3968"
}

resource "aws_lb_listener" "example_listener" {
  load_balancer_arn = aws_lb.example_lb.arn
  port              = 80
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.example_target_group.arn
  }
}

output "load_balancer_dns" {
  value = aws_lb.example_lb.dns_name
}
